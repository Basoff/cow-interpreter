def execute_cow_file(file: str):
    print(f"Выполняется файл \"{file}\"", end=" ")
    f = open(file)
    execute_cow_text(f.read())


def execute_cow_text(text: str):
    instructions = ["moo","mOo","moO","","Moo","MOo","MoO","MOO","OOO","MMM","OOM","oom"]
    array = [0 for i in range(10000)]
    index = 0
    copy = None
    temp = []
    cycles = {}
    commands = []
    i = 0
    while i < len(text)-1:
        com = text[i:i+3]
        if com in ["moo","mOo","moO","mOO","Moo","MOo","MoO","MOO","OOO","MMM","OOM","oom"]:
            commands.append(com)
            i += 3
            if com == "MOO":
                temp.append(len(commands)-1)
            elif com == "moo":
                cycles[len(commands)-1] = temp[-1]
                cycles[temp.pop(-1)] = len(commands)-1  #Добавляем две записи, чтобы и из начала можно было вернуться в конец, и наоборот тоже было можно двигаться.
        else:
            i += 1
        
    print(f"{len(commands)} комманд определено")
    
    i = 0
    output = ""
    while i < len(commands):
        com = commands[i]
        if com == "moo": #Конец цикла
            i = cycles[i]-1
        if com == "mOo": #Шаг влево
            if index == 0:
                break
            else:
                index -= 1
        if com == "moO": #Шаг вправо
            if index == len(array)-1:
                break
            else:
                index += 1
        if com == "mOO": #Выполнить команду номер...
            if array[index] < 0 or array[index] > 11 or array[index] == 3:
                break
            commands[i] = instructions[array[index]]
            i -= 1
        if com == "Moo": #ASCII
            if array[index] == 0:
                array[index] = input()[0]
            else:
                print(chr(array[index]), end="")
        if com == "MOo": #Вычесть 1
            array[index] -= 1
        if com == "MoO": #Прибавить 1
            array[index] += 1
        if com == "MOO": #Начало цикла
            if array[index] == 0:
                i = cycles[i]
        if com == "OOO": #Остановить ноль
            array[index] = 0
        if com == "MMM": #Копипаста
            if copy == None:
                copy = array[index]
            else:
                array[index] = copy
                copy = None
        if com == "OOM": #Вывести число в ячейке
            print(array[index], end="")
        if com == "oom": #Ввести число в ячейку
            array[index] = int(input)
        i += 1
    print(output)

if __name__ == "__main__":
    execute_cow_file('hello.cow')
    execute_cow_file('fib.cow')